package WhileLoopDemo;

import java.util.Scanner;

public class WhileLoopDemo1 {
    public static void main(String[]args)
    {
        Scanner Sc1=new Scanner(System.in);
        int no = 1;
        while (no > 0)
        {
            System.out.println("Enter no");
            no = Sc1.nextInt();
            System.out.println("No");
        }
        System.out.println("Enter negative value");
    }
}
