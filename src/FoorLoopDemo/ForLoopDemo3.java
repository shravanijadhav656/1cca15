package FoorLoopDemo;

import java.util.Scanner;

public class ForLoopDemo3 {
    public static void main(String[]args)
    {
        Scanner Sc1 = new Scanner(System.in);
        System.out.println("Enter start point");
        int start = Sc1.nextInt();
        System.out.println("Enter end point");
        int end = Sc1.nextInt();
        for(int a = end; a<start;a--)
        {
            if (a % 2!=0)
            {
                System.out.println(a);
            }
        }
    }
}
