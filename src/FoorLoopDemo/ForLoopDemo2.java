package FoorLoopDemo;

import java.util.Scanner;

public class ForLoopDemo2 {
    public static void main(String[] args){
        {
            Scanner Sc1 = new Scanner(System.in);
            System.out.println("enter start point");
            int start = Sc1.nextInt ();
            System.out.println("enter end point");
            int end = Sc1.nextInt ();
            for(int a = start;a<end;a++)
            {
                if(a % 2!=0)
                {
                    System.out.println(a);
                }
            }
        }
    }
}
