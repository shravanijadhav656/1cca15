package FoorLoopDemo;

import java.util.Scanner;

public class ForLoopDemo4{
    public static void main(String[]args){
        {
            Scanner Sc1 = new Scanner(System.in);
            System.out.println("Enter start point");
            int start = Sc1.nextInt();
            System.out.println("Enter end point");
            int end = Sc1.nextInt();
            double sum = 0.0;
            for (int a=start;a<=end;a++)
            {
                if (a%2==0)
                {
                    sum+=a;
                }
            }
            System.out.println("sum"+sum);
        }

    }
}
