package DesignPattern;

public class MainApp1 {
    public static void main(String[] args) {
        GoogleAccount acc1=GoogleAccount.login();
        acc1.accessGmail();
        GoogleAccount acc2=GoogleAccount.login();
        acc2.accessDrive();
    }
}
