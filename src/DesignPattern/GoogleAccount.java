package DesignPattern;
//singletone class
public class GoogleAccount {
    //static int count;
    static GoogleAccount g1;
    private GoogleAccount(){
    }
    //singletone method
    static GoogleAccount login(){
        if(g1==null){
            //singletone object
            g1=new GoogleAccount();
            System.out.println("Login Succesfully");
        }
        else{
            System.out.println("Already login");
        }
        return g1;

    }
    void accessGmail(){
        System.out.println("Accessing gmail");
    }
    void accessDrive(){
        System.out.println("Accessing Drive");
    }
}
