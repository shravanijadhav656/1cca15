package String;

public class StringDemo3 {
    public static void main(String[] args){
        {
            String S ="I Love Pune";
            System.out.println(S.length());
            System.out.println(S.charAt(5));
            System.out.println(S.indexOf('e'));
            System.out.println(S.lastIndexOf('a'));
            System.out.println(S.contains("ove"));
            System.out.println(S.startsWith("I"));
            System.out.println(S.endsWith("une"));
            System.out.println(S.substring(6));
            System.out.println(S.substring(0,5));
            System.out.println(S.toUpperCase());
            System.out.println(S.toLowerCase());


        }
    }
}
