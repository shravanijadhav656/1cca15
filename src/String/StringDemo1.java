package String;

public class StringDemo1 {
    public static void main(String[]args)
    {
        // constant pool area
        String S1 = "PUNE";
        String S2 = "MUMBAI";
        String S3 = "PUNE";

        //non constant pool area
        String Str1 = new String("MUMBAI");
        String Str2 = new String("PUNE");
        String Str3 = new String("PUNE");

        System.out.println(S1==S2);//false
        System.out.println(S1== S3);//true
        System.out.println(S1==Str1);//false
        System.out.println(S1.equals(Str2));//true
        System.out.println(Str2.equals(Str3));//false
        System.out.println(S2.equals(Str1));//false
        System.out.println(S2.equalsIgnoreCase(Str1));//true

    }
}
