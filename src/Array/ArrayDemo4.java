package Array;

import java.util.Scanner;

public class ArrayDemo4 {
    public static void main(String[]args){
        {
            Scanner Sc1=new Scanner(System.in);
            System.out.println("enter total no of courses");
            int size=Sc1.nextInt();

            System.out.println("enter marks");
            double[] marks = new double[size];
            double sum=0.0;
            for(int a=0;a<size;a++) {
                marks[a] = Sc1.nextInt();
                sum += marks[a];
            }
            System.out.println("total of marks:"+sum);
            System.out.println("percentage:"+sum/size);
            }
        }
    }

