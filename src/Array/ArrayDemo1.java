package Array;

public class ArrayDemo1 {
    public static void main(String[]args){
        {
            //declaration
            int [] data;

            //size allocation
            data=new int[7];

            //initialization
            data[0]=100;
            data[1]=200;
            data[2]=300;
            data[3]=400;
            data[4]=500;
            data[5]=600;
            data[6]=700;

            //system.out.println(data[0]);
            for (int a=0;a<=6;a++)
            {
                System.out.println(data[a]);
            }

        }
    }
}
