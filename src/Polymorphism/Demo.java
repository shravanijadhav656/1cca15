package Polymorphism;

public abstract class Demo{
    //static & non-static variables
     static int k=20;
     double d=35.25;

     //non static abstract & concrete method
      abstract void test();
      void display(){
          System.out.println("Display method");
      }
      // static concreate methods
        static void info(){
            System.out.println("Info method");
        }
        //constructor
    Demo(){
        System.out.println("Constructor");
    }
    //static & non-static blocks
     static {
        System.out.println("static Blocks");
    }

}
