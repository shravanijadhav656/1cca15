package Casting;

public class MainApp2 {
    public static void main(String[] args) {
        Tv t1;
        t1=new Led();//upcastimg
        t1.displayPicture();

        t1=new Lcd();//upcasting
        t1.displayPicture();
    }
}
