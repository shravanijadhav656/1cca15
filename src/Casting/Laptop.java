package Casting;

public class Laptop extends Machine {
    void getType() {
        System.out.println("Machine type is Laptop");
    }

    @Override
    void calculateBill(int qty, double price) {
        //15% gst
        double total = qty * price;
        double finalAmt = total + total * 0.15;
        System.out.println("Final Amt:" + finalAmt);
    }
}
