package Casting;

public class MainApp {
    public static void main(String[] args) {
        Bike b1=new ElectricalBike();//upcasting
        b1.getType();
        ElectricalBike e=(ElectricalBike) b1;//downcasting
        e.getType();
        e.batteryInfo();

    }
}
