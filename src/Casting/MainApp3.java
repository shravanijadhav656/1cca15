package Casting;

import java.util.Scanner;

public class MainApp3 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter qty");
        int qty=sc1.nextInt();
        System.out.println("Enter price");
        double price= sc1.nextDouble();
        System.out.println("Select Machine");
        System.out.println("1:Laptop\n2:Projecter");
        int choice=sc1.nextInt();
        Machine m1=null;
        if(choice==1) {
            m1 = new Laptop();
        } else if (choice==2) {
            m1=new Projecter();
        }
        m1.getType();
        m1.calculateBill(qty,price);
    }
    }

