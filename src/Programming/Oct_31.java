package Programming;

import java.util.Arrays;

public class Oct_31 {
    public static void main(String[] args) {
        int[] arr = new int[0];
        System.out.println(Arrays.toString(args));

        int max1=arr[0];
        int max2=arr[1];
        for(int i=0;i<arr.length; i++){
            if(arr[i]>max1){
                max2=max1;
                max1=arr[i];

            } else if (arr[i]>max2 && arr[i]!=max1) {
                max2=arr[i];
            }
        }
        System.out.println("Max1:"+max1);
        System.out.println("Max2:"+max2);
    }
}
