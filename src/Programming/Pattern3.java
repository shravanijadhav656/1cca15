package Programming;

public class Pattern3 {
    public static void main(String[] args) {
        int line=7;
        int star=1;

        for(int i=0;i<line;i++)
        {
            for(int j=0;j<star;j++)
                if(j==star-1 || j==star-2)
                System.out.print("*");
            else
                    System.out.print(" ");

            System.out.println();
            if(i<=2)
                star++;
            else
                star--;
        }
        }
    }